package com.eva.service.system;

import com.eva.dao.system.dto.DeleteSystemPermissionDTO;
import com.eva.dao.system.model.SystemPermission;
import com.eva.dao.system.vo.SystemPermissionListVO;

import java.util.List;

/**
 * 系统权限Service定义
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemPermissionService {

    /**
     * 创建
     *
     * @param systemPermission 实体
     * @return Integer
     */
    Integer create(SystemPermission systemPermission);

    /**
     * 主键删除
     *
     * @param id 主键
     */
    void deleteById(Integer id);

    /**
     * 批量主键删除
     *
     * @param ids 主键列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 删除
     *
     * @param dto 删除条件
     */
    void delete (DeleteSystemPermissionDTO dto);

    /**
     * 批量删除
     *
     * @param dtos 删除条件列表
     */
    void deleteInBatch(List<DeleteSystemPermissionDTO> dtos);

    /**
     * 主键更新
     *
     * @param systemPermission 实体
     */
    void updateById(SystemPermission systemPermission);

    /**
     * 批量主键更新
     *
     * @param systemPermissions 实体列表
     */
    void updateByIdInBatch(List<SystemPermission> systemPermissions);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return SystemPermission
     */
    SystemPermission findById(Integer id);

    /**
     * 根据用户ID查询
     *
     * @param userId 用户ID
     * @return List<SystemPermission>
     */
    List<SystemPermission> findByUserId(Integer userId);

    /**
     * 根据角色ID查询
     *
     * @param roleId 角色ID
     * @return List<SystemPermission>
     */
    List<SystemPermission> findByRoleId(Integer roleId);

    /**
     * 条件查询单条记录
     *
     * @param systemPermission 查询条件
     * @return SystemPermission
     */
    SystemPermission findOne(SystemPermission systemPermission);

    /**
     * 条件查询
     *
     * @param systemPermission 查询条件
     * @return List<SystemPermission>
     */
    List<SystemPermission> findList(SystemPermission systemPermission);

    /**
     * 查询管理列表
     *
     * @return List<SystemPermissionListVO>
     */
    List<SystemPermissionListVO> findTree();

    /**
     * 条件统计
     *
     * @param systemPermission 统计条件
     * @return long
     */
    long count(SystemPermission systemPermission);
}
