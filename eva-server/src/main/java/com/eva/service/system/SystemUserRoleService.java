package com.eva.service.system;

import com.eva.core.model.PageData;
import com.eva.core.model.PageWrap;
import com.eva.dao.system.model.SystemUserRole;
import java.util.List;

/**
 * 用户角色关联Service定义
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface SystemUserRoleService {

    /**
     * 创建
     *
     * @param systemUserRole 实体
     * @return Integer
     */
    Integer create(SystemUserRole systemUserRole);

    /**
     * 主键删除
     *
     * @param id 主键
     */
    void deleteById(Integer id);

    /**
     * 删除
     *
     * @param systemUserRole 删除条件
     */
    void delete(SystemUserRole systemUserRole);

    /**
     * 批量主键删除
     *
     * @param ids 主键列表
     */
    void deleteByIdInBatch(List<Integer> ids);

    /**
     * 主键更新
     *
     * @param systemUserRole 实体
     */
    void updateById(SystemUserRole systemUserRole);

    /**
     * 批量主键更新
     *
     * @param systemUserRoles 实体列表
     */
    void updateByIdInBatch(List<SystemUserRole> systemUserRoles);

    /**
     * 主键查询
     *
     * @param id 主键
     * @return SystemUserRole
     */
    SystemUserRole findById(Integer id);

    /**
     * 条件查询单条记录
     *
     * @param systemUserRole 查询条件
     * @return SystemUserRole
     */
    SystemUserRole findOne(SystemUserRole systemUserRole);

    /**
     * 条件查询
     *
     * @param systemUserRole 查询条件
     * @return List<SystemUserRole>
     */
    List<SystemUserRole> findList(SystemUserRole systemUserRole);

    /**
     * 分页查询
     *
     * @param pageWrap 分页参数
     * @return PageData<SystemUserRole>
     */
    PageData<SystemUserRole> findPage(PageWrap<SystemUserRole> pageWrap);

    /**
     * 条件统计
     *
     * @param systemUserRole 统计参数
     * @return long
     */
    long count(SystemUserRole systemUserRole);
}
