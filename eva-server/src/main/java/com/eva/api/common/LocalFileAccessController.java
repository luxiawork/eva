package com.eva.api.common;

import com.eva.api.BaseController;
import com.eva.core.annotation.trace.Trace;
import com.eva.core.constants.ResponseStatus;
import com.eva.core.exception.BusinessException;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Api(tags = "本地文件接口")
@Trace(exclude = true)
@RestController
@RequestMapping("/resource/local")
public class LocalFileAccessController extends BaseController {

    @Value("${resources.import-template:~/files}")
    private String resourcesPath;

    /**
     * 下载本地文件
     */
    @GetMapping("/download")
    public void download (@RequestParam String path, @RequestParam(required = false) String name, HttpServletResponse response) throws IOException {
        File file = new File(resourcesPath.replace("~", new File("").getCanonicalPath()) + path);
        if (!file.exists() || !file.isFile()) {
            throw new BusinessException(ResponseStatus.LOCAL_FILE_NOT_EXISTS);
        }
        ByteArrayOutputStream os = this.getOutputStream(new FileInputStream(file));
        String encodeFileName = URLEncoder.encode(StringUtils.isBlank(name) ? file.getName() : name, StandardCharsets.UTF_8.toString());
        response.setHeader("Content-Disposition","attachment;filename=" + encodeFileName);
        response.setContentType("application/octet-stream");
        response.setHeader("eva-opera-type", "download");
        response.setHeader("eva-download-filename", encodeFileName);
        response.getOutputStream().write(os.toByteArray());
    }
}
