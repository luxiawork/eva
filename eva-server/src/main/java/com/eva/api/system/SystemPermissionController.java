package com.eva.api.system;

import com.eva.api.BaseController;
import com.eva.biz.system.SystemPermissionBiz;
import com.eva.core.annotation.pr.PreventRepeat;
import com.eva.core.model.ApiResponse;
import com.eva.dao.system.dto.DeleteSystemPermissionDTO;
import com.eva.core.constants.Constants;
import com.eva.dao.system.model.SystemPermission;
import com.eva.dao.system.vo.SystemPermissionListVO;
import com.eva.service.system.SystemPermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
@Api(tags = "系统权限")
@RestController
@RequestMapping("/system/permission")
public class SystemPermissionController extends BaseController {

    @Autowired
    private SystemPermissionService systemPermissionService;

    @Autowired
    private SystemPermissionBiz systemPermissionBiz;

    @PreventRepeat
    @ApiOperation("新建")
    @PostMapping("/create")
    @RequiresPermissions("system:permission:create")
    public ApiResponse create(@Validated(Constants.OperaType.Create.class) @RequestBody SystemPermission systemPermission) {
        return ApiResponse.success(systemPermissionBiz.create(systemPermission));
    }

    @ApiOperation("删除")
    @PostMapping("/delete")
    @RequiresPermissions("system:permission:delete")
    public ApiResponse deleteById(@RequestBody DeleteSystemPermissionDTO dto) {
        systemPermissionService.delete(dto);
        return ApiResponse.success(null);
    }

    @ApiOperation("批量删除")
    @PostMapping("/delete/batch")
    @RequiresPermissions("system:permission:delete")
    public ApiResponse deleteByIdInBatch(@RequestBody List<DeleteSystemPermissionDTO> dtos) {
        systemPermissionService.deleteInBatch(dtos);
        return ApiResponse.success(null);
    }

    @ApiOperation("修改")
    @PostMapping("/updateById")
    @RequiresPermissions("system:permission:update")
    public ApiResponse updateById(@Validated(Constants.OperaType.Update.class) @RequestBody SystemPermission systemPermission) {
        systemPermissionBiz.updateById(systemPermission);
        return ApiResponse.success(null);
    }

    @ApiOperation("批量修改")
    @PostMapping("/updateByIdInBatch")
    @RequiresPermissions("system:permission:update")
    public ApiResponse updateById(@RequestBody List<SystemPermission> permissions) {
        systemPermissionService.updateByIdInBatch(permissions);
        return ApiResponse.success(null);
    }

    @ApiOperation("查询权限树列表")
    @GetMapping("/tree")
    @RequiresPermissions("system:permission:query")
    public ApiResponse<List<SystemPermissionListVO>> findAll () {
        return ApiResponse.success(systemPermissionService.findTree());
    }
}
