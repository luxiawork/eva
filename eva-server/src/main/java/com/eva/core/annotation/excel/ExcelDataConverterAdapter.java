package com.eva.core.annotation.excel;

/**
 * Excel数据格式处理适配器
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface ExcelDataConverterAdapter {

    /**
     * 格式化
     *
     * @param args 参数集合，第一个参数为单元格数据
     * @return String
     */
    Object convert(Object... args);
}
