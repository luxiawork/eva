package com.eva.core.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.*;

/**
 * 框架级常量
 * @author Eva.Caesar Liu
 * @since 2022/05/09 19:56
 */
public interface Constants {

    /**
     * 缓存Key
     */
    interface CacheKey {
    }

    /**
     * 操作类型，用于做接口验证分组
     */
    interface OperaType {

        /**
         * 创建
         */
        interface Create {}

        /**
         * 修改
         */
        interface Update {}

        /**
         * 修改状态
         */
        interface UpdateStatus {}
    }

    /**
     * 跟踪日志异常等级
     */
    @Getter
    @AllArgsConstructor
    enum ExceptionLevel {
        DANGER((byte)10, "高"),
        WARN((byte)5, "中"),
        LOW((byte)0, "低"),
        ;

        private byte level;

        private String remark;
    }

    /**
     * MyBatis自动注入忽略
     */
    interface Ignore {
        /**
         * 忽略时间，当不需要更新createTime/updateTime时，可使用该常量进行填充。
         */
        Date IGNORE_TIME = new IgnoreDate();

        /**
         * 忽略用户，当不需要更新createUser/updateUser时，可使用该常量进行填充。
         */
        Integer IGNORE_USER = -1;

        /**
         * 忽略时间类
         */
        class IgnoreDate extends Date {}
    }
}
