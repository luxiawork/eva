package com.eva.dao.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.eva.dao.system.model.SystemMenu;
import com.eva.dao.system.vo.SystemMenuListVO;

import java.util.List;

public interface SystemMenuMapper extends BaseMapper<SystemMenu> {

    /**
     * 查询菜单管理列表
     * 
     * @return List<SystemMenuListVO>
     */
    List<SystemMenuListVO> selectManageList();

    /**
     * 根据用户ID查询
     *
     * @param userId 用户ID
     * @return List<SystemMenu>
     */
    List<SystemMenu> selectByUserId(Integer userId);

    /**
     * 根据角色ID查询
     *
     * @param roleId 角色ID
     * @return List<SystemMenu>
     */
    List<SystemMenu> selectByRoleId(Integer roleId);

}
